/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NEET.test;
import java.io.*;
/**
 *
 * @author Avadhut
 */
public class SerializeTest {
    
    public static void serialize(TestBean testBean)
    {
        try
        {
            FileOutputStream fileOut = new FileOutputStream("test.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(testBean);
            out.close();
            fileOut.close();
        }
        catch(IOException i)
        {
            i.printStackTrace();
        }
    }
    
    public static TestBean deSerialize()
    {
        try
        {
            FileInputStream fileIn = new FileInputStream("test.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            TestBean testBean = (TestBean) in.readObject();
            in.close();
            fileIn.close();
            File f1 = new File("test.ser");
            boolean success = f1.delete();
            if (!success){
                //System.out.println("Deletion failed."); 
            }else{
                //System.out.println("File deleted.");
            }
            return testBean;
        }
        catch(IOException i)
        {
            i.printStackTrace();            
        }
        catch(ClassNotFoundException c)
        {
            //System.out.println("TestBean class not found.");
            c.printStackTrace();            
        }
        return null;        
    }
}