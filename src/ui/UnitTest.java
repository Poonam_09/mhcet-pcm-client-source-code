package ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import JEE.pdfViewer.pdfopener;
import JEE.question.QuestionBean;
import JEE.test.DBConnection;
import JEE.unitTest.UnitTestBean;
import java.awt.Toolkit;

public class UnitTest extends javax.swing.JFrame {

    NewTestForm object;
    private javax.swing.JButton[] jButtonsArray;
    JButton strt = new JButton();
    JButton back = new JButton();
    JButton back1 = new JButton();
    JPanel jp1 = new JPanel();
    JPanel jp = new JPanel();
    int subjectId, CheckBoxNumber, ButtonsNo, div, gg = 0;
    ArrayList<String> nameofchapters, name;
    String subName;
    private javax.swing.JCheckBox[] jCheckboxArray;
    int rollNo;

    /** Creates new form UnitTest */
    public UnitTest(int rollNo) {
        initComponents();
        this.rollNo = rollNo;
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
    }

    public UnitTest(int subid, NewTestForm ob, int rollNo) {
        initComponents();
        this.rollNo = rollNo;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize);
        pnlSubUnitTest.setSize(screenSize);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        object = ob;
        subjectId = subid;
        DBConnection db = new DBConnection();
        CheckBoxNumber = db.noOfChap(subid);
        name = db.nameOfChap(subid);
        jCheckboxArray = new JCheckBox[CheckBoxNumber];
        for (int x = 0; x < CheckBoxNumber; x++) {
            jCheckboxArray[x] = new javax.swing.JCheckBox();
            jCheckboxArray[x].setFont(new java.awt.Font("Verdana", 1, 14));
            jCheckboxArray[x].setText(name.get(x));
            jCheckboxArray[x].setBackground(Color.white);
        }
        if (subid == 1) {
            subName = "Physics";
            div = 12;
        } else if (subid == 2) {
            subName = "Chemistry";
            div = 17;
        } else if (subid == 3) {
            subName = "Maths";

        }
        this.setTitle(subName + ": Unit Test Series");
        GridBagLayout layout = new GridBagLayout();
        pnlSubUnitTest.setLayout(layout);
        jp.setLayout(layout);
        jp1.setLayout(layout);
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 1;
        cons.gridy = 1;
        cons.ipadx = 30;
        cons.ipady = 10;
        cons.anchor = GridBagConstraints.ABOVE_BASELINE;
        layout.setConstraints(jp1, cons);
        jp1.setBackground(Color.red);
        pnlSubUnitTest.add(jp, cons);
        GridBagConstraints cons1 = new GridBagConstraints();
        cons.gridx = 1;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.CENTER;
        cons.fill = new GridBagConstraints().BOTH;
        JLabel title = new JLabel();
        title.setText(subName + " : Select chapters for test.");
        title.setFont(new java.awt.Font("Times New Roman", 1, 22));
        jp.add(title, cons);
        strt.setText("Start");
        back.setText("Back");
        back.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae2) {
                dispose();
                jp.removeAll();
                object.setVisible(true);
            }
        });
        strt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae2) {
                btnStrtActionPerformed(ae2, subjectId, object);
            }
        });
        cons.weightx = cons.weighty = 1;
        cons1.gridx = 2;
        strt.setFont(new java.awt.Font("Times New Roman", 1, 22));
        back.setFont(new java.awt.Font("Times New Roman", 1, 22));
        strt.setSize(subid, gg);
        jp.setBackground(Color.white);
        cons1.gridx = 4;
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        if (subid == 3) {
            cons1.gridx = 4;
            cons.ipadx = 60;
            cons.ipady = 20;
        } else if (subid == 2) {
            cons1.gridx = 3;
            cons.ipadx = 60;
            cons.ipady = 18;
        } else if (subid == 1) {
            cons1.gridx = 3;
            cons.ipadx = 60;
            cons.ipady = 33;
        }
        for (int x = 0; x < CheckBoxNumber; x++) {
            if (x % div == 0) {
                cons.gridx++;
                cons.gridy = 1;
            }
            layout.setConstraints(jCheckboxArray[x], cons);
            jp.add(jCheckboxArray[x], cons);
            cons.gridy++;
        }

        cons.gridx++;
        cons.gridy = 1;
        jp.add(back, cons1, 1);
        cons.gridy = 2;
        jp.add(strt, cons1, 2);
    }

    public UnitTest(int subid, NewTestForm ob, boolean b, boolean b1, int rollNo) {
        initComponents();
        this.rollNo = rollNo;
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        lblTitle.setVisible(false);
        ArrayList<String> name1 = new ArrayList<String>();
        object = ob;
        this.setExtendedState(MAXIMIZED_BOTH);
        if (subid == 3) {
            subName = "Maths";
            div = 17;
            this.setTitle(subName + ": Chapter Summary.");
            ButtonsNo = 34;
            name1.add("1 Sets");
            name1.add("2 Relations");
            name1.add("3 Functions");
            name1.add("4 Complex  Numbers");
            name1.add("5 Quadratic Equations And Inequation");
            name1.add("6 Permutations And Combinations");
            name1.add("7 Mathamatical Induction");
            name1.add("8 Binomial Theorem");
            name1.add("9 Sequances And Series");
            name1.add("10 Cartesian System Of Co-Ordinates And Straight Lines");
            name1.add("11 Family Of Lines");
            name1.add("12 Circles And Systems Of Circles");
            name1.add("13 Parabola");
            name1.add("14 Ellipse");
            name1.add("15  Hyperbola");
            name1.add("16 Statistics");
            name1.add("17 Trigonometric Ratios, Identities And Equations");
            name1.add("18 Solution Of Triangles");
            name1.add("19 Height And Distance");
            name1.add("20 Mathematical  Reasoning");
            name1.add("21 Inverse Trigonometric Function");
            name1.add("22 Determinant");
            name1.add("23 Matrices");
            name1.add("24 Real Numbers");
            name1.add("25 Limit and Continuity");
            name1.add("26 Differentiability & Differentiation");
            name1.add("27 Application Of Derivatives");
            name1.add("28 Indefinite Integrals");
            name1.add("29 Definite Integrals");
            name1.add("30 Area's Under Curves");
            name1.add("31 Differential Equations");
            name1.add("32 Three Dimensional Geometry");
            name1.add("33 Vector Algebra");
            name1.add("34 Probability");
        } else if (subid == 1) {
            subName = "Physics";
            div = 12;
            this.setTitle(subName + ": Chapter Summary.");
            ButtonsNo = 24;
            name1.add("1 Units,Dimensions,Errors Of Measurement");
            name1.add("2 Description of motion in one dimension");
            name1.add("3 Description of motion in two and three dimension");
            name1.add("4 Laws of motion and UCM");
            name1.add("5 Work Power Energy And Collisions");
            name1.add("6 Rotatinal motion");
            name1.add("7 Gravitation");
            name1.add("8 Properties Of Matter");
            name1.add("9 Oscilation");
            name1.add("10 Wave motion");
            name1.add("11 Heat and thermodynamics");
            name1.add("12 Transference of heat");
            name1.add("13 Electrostatics");
            name1.add("14 Current Electricity");
            name1.add("15 Magnetic Effect of current");
            name1.add("16 Magnetostatics");
            name1.add("17 E.M. Induction and A.C.Current");
            name1.add("18 Ray Optics");
            name1.add("19 Wave Optics");
            name1.add("20 Electromagnetic Waves");
            name1.add("21 Electron and Photons");
            name1.add("22 Atoms,Molecules and Nuclei");
            name1.add("23 Solids and Semi-conductor  devices");
            name1.add("24 Communication System");
        } else if (subid == 2) {
            subName = "Chemistry";
            div = 17;
            this.setTitle(subName + ": Chapter Summary.");
            ButtonsNo = 33;
            name1.add("1 Some Basic Concepts Of Chemistry");
            name1.add("2 Structure Of Atom");
            name1.add("3 Classification Of Elements And Periodicity In Properties");
            name1.add("4 Chemical Bonding And Molecular Structure");
            name1.add("5 States Of Matter");
            name1.add("6 Thermodynamics");
            name1.add("7 Chemical Equilibrium");
            name1.add("8 Redox Reactions");
            name1.add("9 Hydrogen");
            name1.add("10 Chemistry Of s-Block Elements");
            name1.add("11 Organic Chemistry Some Basic Concepts");
            name1.add("12 Isomerism In Organic Compounds");
            name1.add("13 PurificationAnd Characterisation Of Organic Compounds");
            name1.add("14 Hydrocarbons");
            name1.add("15 Environmental Pollution");
            name1.add("16 The Solid State");
            name1.add("17 Solution");
            name1.add("18 Electrochemistry");
            name1.add("19 Chemical Kinetics");
            name1.add("20 Surface Chemistry");
            name1.add("21 General Principles And Processes Of Isolation");
            name1.add("22 Chemistry Of  p-Block Elements");
            name1.add("23 Chemistry Of d-And f-Block Elements");
            name1.add("24 Coordination Compounnds");
            name1.add("25 Haloalkanes And Haloarenes");
            name1.add("26 Alcohols, Phenols And Ethers");
            name1.add("27 Aldehydes, Ketone And Carboxylic Acids");
            name1.add("28 Organic Compounds Containing Nitrogen");
            name1.add("29 Polymers");
            name1.add("30 Biomolecules");
            name1.add("31 Chemistry In Everyday");
            name1.add("32 Principles Related To Practical Chemistry");
            name1.add("33 Nuclear Chemistry");
        }
        jButtonsArray = new JButton[ButtonsNo];
        for (int x = 0; x < name1.size(); x++) {
            jButtonsArray[x] = new javax.swing.JButton();
            jButtonsArray[x].setText(name1.get(x));
            jButtonsArray[x].setFont(new java.awt.Font("Verdana", 1, 14));
        }
        GridBagLayout layout = new GridBagLayout();
        pnlSubUnitTest.setLayout(layout);
        jp.setLayout(layout);
        jp1.setLayout(layout);
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 1;
        cons.gridy = 1;
        cons.ipadx = 50;
        cons.ipady = 20;

        cons.anchor = GridBagConstraints.ABOVE_BASELINE;
        layout.setConstraints(jp1, cons);
        //jp.setPreferredSize(new Dimension(900,498));
        jp1.setBackground(Color.red);
        pnlSubUnitTest.add(jp, cons);
        GridBagConstraints cons1 = new GridBagConstraints();
        cons.gridx = 1;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.CENTER;
        cons.fill = new GridBagConstraints().BOTH;
        back1.setText("Back");
        back1.setSize(89, 35);
        back1.setFont(new java.awt.Font("Times New Roman", 1, 22));
        back1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae2) {
                dispose();
                jp.removeAll();
                object.setVisible(true);
            }
        });
        int i;
        for (i = 0; i < name1.size(); i++) {
            final String name = jButtonsArray[i].getText();
            jButtonsArray[i].addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae2) {
                    try {
                        object.dispose();
                        pdfopener pdfo = new pdfopener();
                        pdfo.helpview(name);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "not found");
                    }
                }
            });
        }
        cons.weightx = cons.weighty = 1;
        cons1.gridx = 4;
        jp.add(back1, cons1);
        jp.setBackground(Color.white);
        cons1.gridx = 4;
        cons.gridx = -1;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(5, 80, 8, 0);
        this.add(jp);
        for (int x = 0; x < name1.size(); x++) {
            if (x % div == 0) {
                cons.gridx++;
                cons.gridy = 1;
            }
            layout.setConstraints(jButtonsArray[x], cons);
            jp.add(jButtonsArray[x], cons);
            cons.gridy++;
        }
    }

    private void btnStrtActionPerformed(java.awt.event.ActionEvent evt, int unittestsubid, Object ob) {
        nameofchapters = new ArrayList<String>();
        int i = 0;
        for (int x = 0; x < CheckBoxNumber; x++) {
            if (jCheckboxArray[x].isSelected()) {
                nameofchapters.add(name.get(x));
                String str = nameofchapters.get(i);
                i++;
                System.out.println(str);
            }
        }
        if (nameofchapters.size() > 1 && nameofchapters.size() < 11) {
            JEE.unitTest.DBConnection conn = new JEE.unitTest.DBConnection();
            ArrayList<Integer> chapids = conn.getChapIds(nameofchapters);
            UnitTestBean unitTestBean = conn.getUnitTestBean(unittestsubid, chapids);
            if (unitTestBean != null) {
                object.dispose();
                StartedUnitTestForm t = new StartedUnitTestForm(unitTestBean, true, rollNo);
                t.setVisible(true);
                this.dispose();
            }
        } else {
            JOptionPane.showMessageDialog(null, "At least 2 and maximum 10 chapters are required.");
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitle = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        pnlSubUnitTest = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Scholars Katta's NEET+JEE Software 2014");

        lblTitle.setFont(new java.awt.Font("Verdana", 1, 14));
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("At least 2 and maximum 10 chapters are required.");
        lblTitle.setName("lblTitle"); // NOI18N
        getContentPane().add(lblTitle, java.awt.BorderLayout.CENTER);

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        pnlSubUnitTest.setBackground(new java.awt.Color(255, 255, 255));
        pnlSubUnitTest.setName("pnlSubUnitTest"); // NOI18N
        pnlSubUnitTest.setLayout(new java.awt.GridBagLayout());
        jScrollPane1.setViewportView(pnlSubUnitTest);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.PAGE_START);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(UnitTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(UnitTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(UnitTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(UnitTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
//                new UnitTest().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JPanel pnlSubUnitTest;
    // End of variables declaration//GEN-END:variables
}
