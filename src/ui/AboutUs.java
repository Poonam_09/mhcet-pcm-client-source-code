/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * NewRegistrationForm.java
 *
 * Created on Aug 1, 2014, 2:50:03 PM
 */
package ui;


import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.Timer;


/**
 *
 * @author SAGAR
 */

public class AboutUs extends javax.swing.JFrame {
    int rollNo=0;
    CardLayout cl;
    static Timer timer;
    final URI uri = new URI("http://www.dhaygudewadischool.com");
    final URI uri1 = new URI("http://www.delwadi.com");
    final URI uri2 = new URI("http://www.udgirkarphysicsclasses.com/");
    final URI uri3 = new URI("http://www.crunchersoft.com"); 
    final URI uri4 = new URI("http://www.cetonlinetest.com");
    NewTestForm newTestform;
    /** Creates new form NewRegistrationForm */
    public AboutUs(int rollNo,NewTestForm newTestform) throws URISyntaxException {
        initComponents();
        setLocation(0,0);
        cl = (CardLayout) home.getLayout();
        this.getContentPane().setBackground(Color.white);
        this.rollNo=rollNo;
        this.newTestform=newTestform;
        cl.show(home, "card2");
//        btnBack.setVisible(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screenSize.width, (screenSize.height * 95) / 100);
//        jPanel1.setSize(screenSize);
        lblcombo1.setText("<html><FONT color=\\\"#000099\\\">Features</FONT><br />- Included latest syllabus of MH-CET. <br />- Chapter - Wise, Unit - Wise, subject - Wise test series. <br />- Level wise, Numerical & Theoretical Test Series <br />- Previous 15 years Question Papers with Solutions  <br />- Chapter wise weightage analysis of previous years <br />- Periodic Table with audio & graphical representation.  <br />- Performance Graph shows students preparation level. <br />- Hints available for almost all difficult questions.  <br />- Effective chapter summary. <br />- Includes 100+ career options.<br />- For more visit on www.cetonlinetest.com<html>");
        lblcombo.setText("<html><FONT color=\"#000099\">Features</FONT><br />- Easy to learn & Runs without internet connection. <br />- Included latest syllabus of MH-CET. <br />- Chapter - Wise, Unit - Wise, subject - Wise test series. <br />- Level wise, Numerical & Theoretical Test Series <br />- Previous 15 years Question Papers with Solutions  <br />- Chapter wise weightage analysis of previous years <br />- Periodic Table with audio & graphical representation.  <br />- Performance Graph shows students preparation level. <br />- Hints available for almost all difficult questions.  <br />- Effective chapter summary. <br />- Includes 100+ career options.<html>");
        lbljee.setText("<html><FONT color=\"#000099\">Features</FONT><br />- Easy to learn & Runs without internet connection. <br />- Included latest syllabus of JEE CET.  <br />- Chapter - Wise, Unit - Wise, subject - Wise test series. <br />- Level wise, Numerical & Theoretical Test <br />- Previous 10 years Question Papers with Solutions. <br />- Chapter wise weightage analysis of previous exams. <br />- Periodic Table with audio & graphical representation. <br />- Performance Graph shows students preparation level. <br />- Hints available for almost all difficult questions. <br />- Covers previous 10 year's competitive exam questions. <br />- Effective chapter summary. <br />- Includes 100+career options. <html>");
        lblcs.setText("<html><FONT color=\"#000099\">Features</FONT><br />- Easy to learn & Runs without internet connection. <br />- Included latest syllabus of Medical CET. <br />- More than 25,000 multiple choice questions of PCM. <br />- Chapter - Wise, Unit - Wise, subject - Wise test series. <br />- Level wise, Numerical & Theoretical Test Series. <br />- Previous 10 years Question Papers with Solutions. <br />- Chapter wise weightage analysis of previous exams. <br />- Periodic Table with audio & graphical representation. <br />- Performance Graph shows students preparation level. <br />- Hints available for almost all difficult questions. <br />- Covers previous 10 year's competitive exam questions. <br />- Effective chapter summary. <br />- Includes 100+career options. <html>");
        lblerp.setText("<html><FONT color=\"#000099\">Features</FONT><br />- Billing <br />- Purchase<br />- Sell <br />- Rent <br />- Stock<br />- Accounting <br />- Employee Details <br />- Employee Salary <br />- Personal Expenditure<br />- Reminder<br />- Bank transaction <br />-  Business Growth Report<br />-  All Reports<html>");
        lblcs.setText("<html><FONT color=\\\"#000099\\\">Features</FONT><br />\n" +
"- User friendly runs without internet connection.<br />\n" +
"- Inbuilt 45,000 MCQ’s of PCMB selected by experts. <br />\n" +
"- Add, Delete and Modify options for subject, chapter and questions on server side. <br />\n" +
"- Server can distribute following test<br />\n" +
"&nbsp &nbsp  *Chapter wise &nbsp &nbsp  *Subject wise<br />\n" +
"&nbsp &nbsp  *Unit wise &nbsp &nbsp &nbsp  *Group wise<br />\n" +
"- Advance options for test<br />\n" +
"&nbsp &nbsp  *Server can easily create, save, delete, redistribute test with time limit.<br />\n" +
"&nbsp &nbsp  *Shuffle question(To change sequence of question for each client)<br />\n" +
"&nbsp &nbsp  *Paper Code (ex. A,B,C,D)<br />\n" +
"&nbsp &nbsp  *Question Sort By <br />\n" +
"&nbsp &nbsp  *Level(Easy, Medium, Hard)<br />\n" +
"&nbsp &nbsp &nbsp &nbsp #Type(Theory, Numerical)  &nbsp #Used, Not-Used &nbsp #Previously Asked<br />\n" +
"- Server can view submitted test of particular client.<br />\n" +
"- Server can generate separate result of each client.<br />\n" +
"- Reports- Rank–wise, Separate client-wise,Test–wise,Progress report<br />\n" +
"- Each client have separate Login Id and password according to registration. <br />\n" +
"- Customization will be provided as per the requirement with additional charges.<br /> <br /> <html>");
   lblsc.setText("<html><FONT color=\\\"#000099\\\">Features</FONT><br />\n" +
"- User friendly runs without internet connection.<br />\n" +
"- Student Details -Add, Delete and Modify student details. <br />\n" +
"- Roll-no Generator - Male-First, Female-First, New Student.<br />\n" +
"- Class Transfer -Transfer students from one class to another.<br />\n" +
"- Fees management<br />\n" +
"&nbsp &nbsp  *Student Fees collection, Pending Fees records, <br />\n" +
"&nbsp &nbsp  *SMS sending to parents regarding fees.<br />\n" +
"- OMR Reader<br />\n" +
"&nbsp &nbsp  *Automatically checking of OMR-Answer Sheets by following ways:<br />\n" +
"&nbsp &nbsp  *Answer-Sheet- Add, Delete, Modify correct answer sheet.<br />\n" +
"&nbsp &nbsp  *Check-Answer Sheet -Automatically generates results of all students.<br />\n" +
"&nbsp &nbsp  *Type - 50 and 200 questions answer sheet.<br />\n" +
"&nbsp &nbsp  *Bonus Marks - Add any kind of bonus marks.<br />\n" +
"- Reports <br />\n" +
"&nbsp &nbsp  *Student List, Student Attendance Report, <br />\n" +
"&nbsp &nbsp  *Student Mark-sheet, Student Progress report.<br />\n" +
"- Answer Sheet Micro Report- Percentage of students who <br />\n" +
"&nbsp &nbsp  *solved correct and incorrect answers.<br />\n" +
"- I-Card Generation- with different customize options.<br />\n" +
"- Attendance Management with auto-SMS sending.<br />\n" +
"- SMS-Facility - Result SMS, Attendance SMS, Fees SMS, Bulk SMS.<br /> <br /> <html>");
        lblprint.setText("<html><FONT color=\\\"#000099\\\">Features</FONT><br />\n" +
"-User friendly, Run without internet connection.<br />\n" +
"-Built-in Question MCQ's selected by experts.<br />\n" +
"-Create question paper of single chapter, multiple  <br />\n" +
" &nbsp chapter,subject-wise, group-wise and paper wise.<br />\n" +
"-Paper size options : A4, Legal, Letter.<br />\n" +
"-Also create blank OMR sheet, correct answer sheet.<br />\n" +
"-Question sorting options for printing<br />\n" +
"&nbsp &nbsp  * Level-wise: Easy/Medium/Hard<br />\n" +
"&nbsp &nbsp  * Type-wise: Theoretical/ Numerical<br />\n" +
"&nbsp &nbsp * Status wise: Used/ Unused questions<br />\n" +
"-Add, delete, modify questions.<br />\n" +
"-Customized options for Header, Footer, Watermark<br />\n" +
                " &nbsp and more.<br />\n" +
"-Create question paper of MH-CET(2000-2014)<br />\n" +
"-Detailed chapter summary and career guidance.<br />\n" +
"-1 year free technical support. <br /> <html>");
        lbltable.setText("<html><FONT color=\"#000099\">Features</FONT><br />- No need of internet connection. <br />- User friendly. <br />- Create Time table for classes in schools or colleges. <br />- Two way timetable creation system  Class wise <br />- Staff Wise <br />- Holiday Reminder<br />- Print Reports <br />-  Class wise time table<br />- Staff wise timetable <br />- Manage every staff’s member timetable <br />- Easy to use and Handle<html>");
        btnweb0.setText("<HTML><U>Dhaygudewadi School</U><HTML>");
        btnweb1.setText("<HTML><U>Delvadi Village</U><HTML>");
        btnweb3.setText("<HTML><U>www.cetonlinetest.com</U><HTML>");
        btnweb2.setText("<HTML><U>Udgirkar Physics Classes</U><HTML>");
        btnweb1.setText("<HTML><U>Delwadi Village</U><HTML>");
        btnwebsite.setText("<HTML><U><FONT color=\"#000099\">www.crunchersoft.com</FONT></U><HTML>");
//        lbladdress.setText("<html>CruncherSoft Technologies Pvt. Ltd. Technologies Pvt. Ltd. <br />3rd floor, Office No. 3, <br />Opp. to Reliance Mart, <br />J.P.Building, Tukaram Nagar, <br />Kharadi, Pune. - 411 014 <br /> Maharashtra, India. <br />CruncherSoft Technologies Pvt. Ltd.@gmail.com<br />020-65404041<html>");
        lbladdress.setText("<html>CruncherSoft Technologies Pvt. Ltd. <br /> <br />7th floor, office no.16 ,City vista, oppo. Victorious School,Near WTC, <br />Ashoka Nagar, <br />Kharadi, Pune. - 411 014 <br /> Maharashtra, India. <br />crunchersoft@gmail.com<br />8975626060<html>");
        lblaboutus.setText("<html>&nbsp &nbsp &nbsp  CruncherSoft Technologies Pvt. Ltd. is one of the best service providing company in Pune, Maharashtra. If you are looking for a professional and creative software developers and web designers for your software or websites then CruncherSoft Technologies Pvt. Ltd. is the final place to fulfill your needs. <br />&nbsp &nbsp &nbsp CruncherSoft Technologies Pvt. Ltd. is growing IT company which is providing software products and excellent services in custom software, web development, e-commerce portals and IT services. <br />&nbsp &nbsp &nbsp We have team of software professionals with better experience in different fields. Our developers keep themselves updated with latest technology and trends in market. Our skilled peoples have their own objectives with respect to their teams which will ultimately contribute to achieve the organization’s goals. CruncherSoft Technologies Pvt. Ltd. is a young start-up company having desire to be the best in IT industry by providing excellent IT services and creating unique software products. <br />&nbsp &nbsp &nbsp Our slogan is, “YOU PLAN WE IMPLEMENT’’, if you are thinking of doing something with us we will give you more than you expected. <br />&nbsp &nbsp &nbsp Our Goal is to become a best in the world by delivering innovative ideas through software. <html>");

        int timerTimeInMilliSeconds = 1500;
        timer = new javax.swing.Timer(timerTimeInMilliSeconds, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                timer.stop();
            }
        });
        this.setLocationRelativeTo(null);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        
        
        lblStudentPCM.setText("<HTML>(29,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentPCMB.setText("<HTML>(43,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentPCB.setText("<HTML>(34,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentP.setText("<HTML>(11,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentC.setText("<HTML>(9,000 MCQ's) <br />  &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentM.setText("<HTML>(9,000 MCQ's) <br />  &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentB.setText("<HTML>(14,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        
        lblPRPCM.setText("<HTML>(29,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 40,000/-<HTML>");
        lblPRPCMB.setText("<HTML>(43,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 50,000/-<HTML>");
        lblPRPCB.setText("<HTML>(34,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 40,000/-<HTML>");
        lblPRP.setText("<HTML>(11,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 15,000/-<HTML>");
        lblPRC.setText("<HTML>(9,000 MCQ's) <br />  &nbsp &nbsp &nbsp &nbsp 15,000/-<HTML>");
        lblPRM.setText("<HTML>(9,000 MCQ's) <br />  &nbsp &nbsp &nbsp &nbsp 15,000/-<HTML>");
        lblPRB.setText("<HTML>(14,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 15,000/-<HTML>");
        
        
        lbljeee.setText("<HTML>(28,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblneett.setText("<HTML>(35,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        
        
        
        
    }

     public AboutUs(int rollNo) throws URISyntaxException {
        initComponents();
        setLocation(0,0);
        cl = (CardLayout) home.getLayout();
        this.getContentPane().setBackground(Color.white);
        this.rollNo=rollNo;
        this.newTestform=newTestform;
        cl.show(home, "card2");
//        btnBack.setVisible(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screenSize.width, (screenSize.height * 95) / 100);
//        jPanel1.setSize(screenSize);
        lblcombo1.setText("<html><FONT color=\\\"#000099\\\">Features</FONT><br />- Included latest syllabus of MH-CET. <br />- Chapter - Wise, Unit - Wise, subject - Wise test series. <br />- Level wise, Numerical & Theoretical Test Series <br />- Previous 15 years Question Papers with Solutions  <br />- Chapter wise weightage analysis of previous years <br />- Periodic Table with audio & graphical representation.  <br />- Performance Graph shows students preparation level. <br />- Hints available for almost all difficult questions.  <br />- Effective chapter summary. <br />- Includes 100+ career options.<br />- For more visit on www.cetonlinetest.com<html>");
        lblcombo.setText("<html><FONT color=\"#000099\">Features</FONT><br />- Easy to learn & Runs without internet connection. <br />- Included latest syllabus of MH-CET. <br />- Chapter - Wise, Unit - Wise, subject - Wise test series. <br />- Level wise, Numerical & Theoretical Test Series <br />- Previous 15 years Question Papers with Solutions  <br />- Chapter wise weightage analysis of previous years <br />- Periodic Table with audio & graphical representation.  <br />- Performance Graph shows students preparation level. <br />- Hints available for almost all difficult questions.  <br />- Effective chapter summary. <br />- Includes 100+ career options.<html>");
        lbljee.setText("<html><FONT color=\"#000099\">Features</FONT><br />- Easy to learn & Runs without internet connection. <br />- Included latest syllabus of JEE CET.  <br />- Chapter - Wise, Unit - Wise, subject - Wise test series. <br />- Level wise, Numerical & Theoretical Test <br />- Previous 10 years Question Papers with Solutions. <br />- Chapter wise weightage analysis of previous exams. <br />- Periodic Table with audio & graphical representation. <br />- Performance Graph shows students preparation level. <br />- Hints available for almost all difficult questions. <br />- Covers previous 10 year's competitive exam questions. <br />- Effective chapter summary. <br />- Includes 100+career options. <html>");
        lblcs.setText("<html><FONT color=\"#000099\">Features</FONT><br />- Easy to learn & Runs without internet connection. <br />- Included latest syllabus of Medical CET. <br />- More than 25,000 multiple choice questions of PCM. <br />- Chapter - Wise, Unit - Wise, subject - Wise test series. <br />- Level wise, Numerical & Theoretical Test Series. <br />- Previous 10 years Question Papers with Solutions. <br />- Chapter wise weightage analysis of previous exams. <br />- Periodic Table with audio & graphical representation. <br />- Performance Graph shows students preparation level. <br />- Hints available for almost all difficult questions. <br />- Covers previous 10 year's competitive exam questions. <br />- Effective chapter summary. <br />- Includes 100+career options. <html>");
        lblerp.setText("<html><FONT color=\"#000099\">Features</FONT><br />- Billing <br />- Purchase<br />- Sell <br />- Rent <br />- Stock<br />- Accounting <br />- Employee Details <br />- Employee Salary <br />- Personal Expenditure<br />- Reminder<br />- Bank transaction <br />-  Business Growth Report<br />-  All Reports<html>");
        lblcs.setText("<html><FONT color=\\\"#000099\\\">Features</FONT><br />\n" +
"- User friendly runs without internet connection.<br />\n" +
"- Inbuilt 45,000 MCQ’s of PCMB selected by experts. <br />\n" +
"- Add, Delete and Modify options for subject, chapter and questions on server side. <br />\n" +
"- Server can distribute following test<br />\n" +
"&nbsp &nbsp  *Chapter wise &nbsp &nbsp  *Subject wise<br />\n" +
"&nbsp &nbsp  *Unit wise &nbsp &nbsp &nbsp  *Group wise<br />\n" +
"- Advance options for test<br />\n" +
"&nbsp &nbsp  *Server can easily create, save, delete, redistribute test with time limit.<br />\n" +
"&nbsp &nbsp  *Shuffle question(To change sequence of question for each client)<br />\n" +
"&nbsp &nbsp  *Paper Code (ex. A,B,C,D)<br />\n" +
"&nbsp &nbsp  *Question Sort By <br />\n" +
"&nbsp &nbsp  *Level(Easy, Medium, Hard)<br />\n" +
"&nbsp &nbsp &nbsp &nbsp #Type(Theory, Numerical)  &nbsp #Used, Not-Used &nbsp #Previously Asked<br />\n" +
"- Server can view submitted test of particular client.<br />\n" +
"- Server can generate separate result of each client.<br />\n" +
"- Reports- Rank–wise, Separate client-wise,Test–wise,Progress report<br />\n" +
"- Each client have separate Login Id and password according to registration. <br />\n" +
"- Customization will be provided as per the requirement with additional charges.<br /> <br /> <html>");
   lblsc.setText("<html><FONT color=\\\"#000099\\\">Features</FONT><br />\n" +
"- User friendly runs without internet connection.<br />\n" +
"- Student Details -Add, Delete and Modify student details. <br />\n" +
"- Roll-no Generator - Male-First, Female-First, New Student.<br />\n" +
"- Class Transfer -Transfer students from one class to another.<br />\n" +
"- Fees management<br />\n" +
"&nbsp &nbsp  *Student Fees collection, Pending Fees records, <br />\n" +
"&nbsp &nbsp  *SMS sending to parents regarding fees.<br />\n" +
"- OMR Reader<br />\n" +
"&nbsp &nbsp  *Automatically checking of OMR-Answer Sheets by following ways:<br />\n" +
"&nbsp &nbsp  *Answer-Sheet- Add, Delete, Modify correct answer sheet.<br />\n" +
"&nbsp &nbsp  *Check-Answer Sheet -Automatically generates results of all students.<br />\n" +
"&nbsp &nbsp  *Type - 50 and 200 questions answer sheet.<br />\n" +
"&nbsp &nbsp  *Bonus Marks - Add any kind of bonus marks.<br />\n" +
"- Reports <br />\n" +
"&nbsp &nbsp  *Student List, Student Attendance Report, <br />\n" +
"&nbsp &nbsp  *Student Mark-sheet, Student Progress report.<br />\n" +
"- Answer Sheet Micro Report- Percentage of students who <br />\n" +
"&nbsp &nbsp  *solved correct and incorrect answers.<br />\n" +
"- I-Card Generation- with different customize options.<br />\n" +
"- Attendance Management with auto-SMS sending.<br />\n" +
"- SMS-Facility - Result SMS, Attendance SMS, Fees SMS, Bulk SMS.<br /> <br /> <html>");
        lblprint.setText("<html><FONT color=\\\"#000099\\\">Features</FONT><br />\n" +
"-User friendly, Run without internet connection.<br />\n" +
"-Built-in Question MCQ's selected by experts.<br />\n" +
"-Create question paper of single chapter, multiple  <br />\n" +
" &nbsp chapter,subject-wise, group-wise and paper wise.<br />\n" +
"-Paper size options : A4, Legal, Letter.<br />\n" +
"-Also create blank OMR sheet, correct answer sheet.<br />\n" +
"-Question sorting options for printing<br />\n" +
"&nbsp &nbsp  * Level-wise: Easy/Medium/Hard<br />\n" +
"&nbsp &nbsp  * Type-wise: Theoretical/ Numerical<br />\n" +
"&nbsp &nbsp * Status wise: Used/ Unused questions<br />\n" +
"-Add, delete, modify questions.<br />\n" +
"-Customized options for Header, Footer, Watermark<br />\n" +
                " &nbsp and more.<br />\n" +
"-Create question paper of MH-CET(2000-2014)<br />\n" +
"-Detailed chapter summary and career guidance.<br />\n" +
"-1 year free technical support. <br /> <html>");
        lbltable.setText("<html><FONT color=\"#000099\">Features</FONT><br />- No need of internet connection. <br />- User friendly. <br />- Create Time table for classes in schools or colleges. <br />- Two way timetable creation system  Class wise <br />- Staff Wise <br />- Holiday Reminder<br />- Print Reports <br />-  Class wise time table<br />- Staff wise timetable <br />- Manage every staff’s member timetable <br />- Easy to use and Handle<html>");
        btnweb0.setText("<HTML><U>Dhaygudewadi School</U><HTML>");
        btnweb1.setText("<HTML><U>Delvadi Village</U><HTML>");
        btnweb3.setText("<HTML><U>www.cetonlinetest.com</U><HTML>");
        btnweb2.setText("<HTML><U>Udgirkar Physics Classes</U><HTML>");
        btnweb1.setText("<HTML><U>Delwadi Village</U><HTML>");
        btnwebsite.setText("<HTML><U><FONT color=\"#000099\">www.crunchersoft.com</FONT></U><HTML>");
//        lbladdress.setText("<html>CruncherSoft Technologies Pvt. Ltd. Technologies Pvt. Ltd. <br />3rd floor, Office No. 3, <br />Opp. to Reliance Mart, <br />J.P.Building, Tukaram Nagar, <br />Kharadi, Pune. - 411 014 <br /> Maharashtra, India. <br />CruncherSoft Technologies Pvt. Ltd.@gmail.com<br />020-65404041<html>");
       lbladdress.setText("<html>CruncherSoft Technologies Pvt. Ltd. Technologies Pvt. Ltd. <br /> <br />7th floor, office no.16 ,City vista, oppo. Victorious School,Near WTC, <br />Ashoka Nagar, <br />Kharadi, Pune. - 411 014 <br /> Maharashtra, India. <br />crunchersoft@gmail.com<br />8975626060<html>");
       lblaboutus.setText("<html>&nbsp &nbsp &nbsp  CruncherSoft Technologies Pvt. Ltd. is one of the best service providing company in Pune, Maharashtra. If you are looking for a professional and creative software developers and web designers for your software or websites then CruncherSoft Technologies Pvt. Ltd. is the final place to fulfill your needs. <br />&nbsp &nbsp &nbsp CruncherSoft Technologies Pvt. Ltd. is growing IT company which is providing software products and excellent services in custom software, web development, e-commerce portals and IT services. <br />&nbsp &nbsp &nbsp We have team of software professionals with better experience in different fields. Our developers keep themselves updated with latest technology and trends in market. Our skilled peoples have their own objectives with respect to their teams which will ultimately contribute to achieve the organization’s goals. CruncherSoft Technologies Pvt. Ltd. is a young start-up company having desire to be the best in IT industry by providing excellent IT services and creating unique software products. <br />&nbsp &nbsp &nbsp Our slogan is, “YOU PLAN WE IMPLEMENT’’, if you are thinking of doing something with us we will give you more than you expected. <br />&nbsp &nbsp &nbsp Our Goal is to become a best in the world by delivering innovative ideas through software. <html>");

        int timerTimeInMilliSeconds = 1500;
        timer = new javax.swing.Timer(timerTimeInMilliSeconds, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                timer.stop();
            }
        });
        this.setLocationRelativeTo(null);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        
        
        lblStudentPCM.setText("<HTML>(29,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentPCMB.setText("<HTML>(43,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentPCB.setText("<HTML>(34,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentP.setText("<HTML>(11,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentC.setText("<HTML>(9,000 MCQ's) <br />  &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentM.setText("<HTML>(9,000 MCQ's) <br />  &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblStudentB.setText("<HTML>(14,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        
        lblPRPCM.setText("<HTML>(29,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 40,000/-<HTML>");
        lblPRPCMB.setText("<HTML>(43,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 50,000/-<HTML>");
        lblPRPCB.setText("<HTML>(34,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 40,000/-<HTML>");
        lblPRP.setText("<HTML>(11,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 15,000/-<HTML>");
        lblPRC.setText("<HTML>(9,000 MCQ's) <br />  &nbsp &nbsp &nbsp &nbsp 15,000/-<HTML>");
        lblPRM.setText("<HTML>(9,000 MCQ's) <br />  &nbsp &nbsp &nbsp &nbsp 15,000/-<HTML>");
        lblPRB.setText("<HTML>(14,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp 15,000/-<HTML>");
        
        
        lbljeee.setText("<HTML>(28,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        lblneett.setText("<HTML>(35,000 MCQ's) <br /> &nbsp &nbsp &nbsp &nbsp <HTML>");
        
        
        
        
    }
    private static void open(URI uri) {
        if (Desktop.isDesktopSupported()) {
            try {
                Desktop.getDesktop().browse(uri);
            } catch (IOException e) { /* TODO: error handling */ }
        } else { /* TODO: error handling */ }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        BackBtnPanel = new javax.swing.JPanel();
        LblMainBack = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        home = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        LblBack = new javax.swing.JLabel();
        combo = new javax.swing.JPanel();
        lblcombo = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lblStudentPCB = new javax.swing.JLabel();
        lblStudentPCMB = new javax.swing.JLabel();
        lblStudentPCM = new javax.swing.JLabel();
        lblStudentB = new javax.swing.JLabel();
        lblStudentM = new javax.swing.JLabel();
        lblStudentC = new javax.swing.JLabel();
        lblStudentP = new javax.swing.JLabel();
        product = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jee = new javax.swing.JPanel();
        lbljeee = new javax.swing.JLabel();
        lbljee = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        backjee = new javax.swing.JButton();
        lblneett = new javax.swing.JLabel();
        Clientserver = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        lblcs = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        printing = new javax.swing.JPanel();
        lblPRPCMB = new javax.swing.JLabel();
        lblprint = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        backpaper = new javax.swing.JButton();
        lblPRPCB = new javax.swing.JLabel();
        lblPRPCM = new javax.swing.JLabel();
        lblPRB = new javax.swing.JLabel();
        lblPRC = new javax.swing.JLabel();
        lblPRM = new javax.swing.JLabel();
        lblPRP = new javax.swing.JLabel();
        school = new javax.swing.JPanel();
        lblsc = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        timetable = new javax.swing.JPanel();
        lbltable = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        erp = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        lblerp = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jButton8 = new javax.swing.JButton();
        website = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jButton9 = new javax.swing.JButton();
        btnweb1 = new javax.swing.JButton();
        btnweb0 = new javax.swing.JButton();
        btnweb2 = new javax.swing.JButton();
        contactus = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        lbladdress = new javax.swing.JLabel();
        btnbackaddress = new javax.swing.JButton();
        btnwebsite = new javax.swing.JButton();
        aboutus = new javax.swing.JPanel();
        lblaboutus = new javax.swing.JLabel();
        btnbackaboutus = new javax.swing.JButton();
        cetonline = new javax.swing.JPanel();
        lblcombo1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        btnbackonline = new javax.swing.JButton();
        btnweb3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("CruncherSoft's JEE Preparation Software 2014");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(0, 53, 68));
        jPanel4.setName("jPanel4"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Microsoft JhengHei", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("About Us");
        jLabel1.setName("jLabel1"); // NOI18N

        BackBtnPanel.setBackground(new java.awt.Color(0, 53, 68));
        BackBtnPanel.setMaximumSize(new java.awt.Dimension(80, 70));
        BackBtnPanel.setMinimumSize(new java.awt.Dimension(80, 70));
        BackBtnPanel.setName("BackBtnPanel"); // NOI18N

        LblMainBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Back-1.png"))); // NOI18N
        LblMainBack.setName("LblMainBack"); // NOI18N
        LblMainBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblMainBackMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblMainBackMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblMainBackMouseExited(evt);
            }
        });

        javax.swing.GroupLayout BackBtnPanelLayout = new javax.swing.GroupLayout(BackBtnPanel);
        BackBtnPanel.setLayout(BackBtnPanelLayout);
        BackBtnPanelLayout.setHorizontalGroup(
            BackBtnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BackBtnPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblMainBack)
                .addContainerGap())
        );
        BackBtnPanelLayout.setVerticalGroup(
            BackBtnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BackBtnPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(LblMainBack)
                .addGap(5, 5, 5))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(BackBtnPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(628, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BackBtnPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25))
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setName("jPanel5"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        home.setName("home"); // NOI18N
        home.setPreferredSize(new java.awt.Dimension(1100, 900));
        home.setLayout(new java.awt.CardLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setName("jPanel2"); // NOI18N

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/CompanyLogo.png"))); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/Clients.png"))); // NOI18N
        jLabel23.setName("jLabel23"); // NOI18N

        jLabel25.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 18)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(2, 168, 158));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("Our Clients");
        jLabel25.setName("jLabel25"); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setName("jPanel1"); // NOI18N

        jLabel2.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 28)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("  Contact Us");
        jLabel2.setName("jLabel2"); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel2MouseExited(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 28)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("  About Company");
        jLabel5.setName("jLabel5"); // NOI18N
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel5MouseExited(evt);
            }
        });

        LblBack.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 28)); // NOI18N
        LblBack.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBack.setText("  Company Products");
        LblBack.setName("LblBack"); // NOI18N
        LblBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblBackMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblBackMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblBackMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblBack, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(185, 185, 185)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(214, 214, 214)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblBack, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23)
                .addGap(0, 0, 0))
        );

        home.add(jPanel2, "card2");

        combo.setBackground(new java.awt.Color(255, 255, 255));
        combo.setName("combo"); // NOI18N

        lblcombo.setBackground(new java.awt.Color(255, 255, 255));
        lblcombo.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 18)); // NOI18N
        lblcombo.setForeground(new java.awt.Color(0, 143, 207));
        lblcombo.setName("lblcombo"); // NOI18N

        jLabel8.setBackground(new java.awt.Color(204, 204, 255));
        jLabel8.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel8.setText("MH CET Preparation Software ");
        jLabel8.setName("jLabel8"); // NOI18N

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        jButton1.setBorder(null);
        jButton1.setBorderPainted(false);
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton1MouseExited(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel3.setBackground(java.awt.Color.white);
        jPanel3.setName("jPanel3"); // NOI18N

        lblStudentPCB.setBackground(new java.awt.Color(255, 255, 255));
        lblStudentPCB.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblStudentPCB.setForeground(new java.awt.Color(255, 0, 0));
        lblStudentPCB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PCB.png"))); // NOI18N
        lblStudentPCB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblStudentPCB.setName("lblStudentPCB"); // NOI18N
        lblStudentPCB.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblStudentPCMB.setBackground(new java.awt.Color(255, 255, 255));
        lblStudentPCMB.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblStudentPCMB.setForeground(new java.awt.Color(255, 0, 0));
        lblStudentPCMB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PCMB.png"))); // NOI18N
        lblStudentPCMB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblStudentPCMB.setName("lblStudentPCMB"); // NOI18N
        lblStudentPCMB.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblStudentPCM.setBackground(new java.awt.Color(255, 255, 255));
        lblStudentPCM.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblStudentPCM.setForeground(new java.awt.Color(255, 0, 0));
        lblStudentPCM.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PCM.png"))); // NOI18N
        lblStudentPCM.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblStudentPCM.setName("lblStudentPCM"); // NOI18N
        lblStudentPCM.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblStudentB.setBackground(new java.awt.Color(255, 255, 255));
        lblStudentB.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblStudentB.setForeground(new java.awt.Color(255, 0, 0));
        lblStudentB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/bio.png"))); // NOI18N
        lblStudentB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblStudentB.setName("lblStudentB"); // NOI18N
        lblStudentB.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblStudentM.setBackground(new java.awt.Color(255, 255, 255));
        lblStudentM.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblStudentM.setForeground(new java.awt.Color(255, 0, 0));
        lblStudentM.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/math.png"))); // NOI18N
        lblStudentM.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblStudentM.setName("lblStudentM"); // NOI18N
        lblStudentM.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblStudentC.setBackground(new java.awt.Color(255, 255, 255));
        lblStudentC.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblStudentC.setForeground(new java.awt.Color(255, 0, 0));
        lblStudentC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/che.png"))); // NOI18N
        lblStudentC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblStudentC.setName("lblStudentC"); // NOI18N
        lblStudentC.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblStudentP.setBackground(new java.awt.Color(255, 255, 255));
        lblStudentP.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblStudentP.setForeground(new java.awt.Color(255, 0, 0));
        lblStudentP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/phy.png"))); // NOI18N
        lblStudentP.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblStudentP.setName("lblStudentP"); // NOI18N
        lblStudentP.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(lblStudentP)
                .addGap(10, 10, 10)
                .addComponent(lblStudentC)
                .addGap(10, 10, 10)
                .addComponent(lblStudentM)
                .addGap(10, 10, 10)
                .addComponent(lblStudentB)
                .addGap(23, 23, 23))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(106, 106, 106)
                .addComponent(lblStudentPCM)
                .addGap(15, 15, 15)
                .addComponent(lblStudentPCMB)
                .addGap(15, 15, 15)
                .addComponent(lblStudentPCB)
                .addGap(106, 106, 106))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(lblStudentPCMB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblStudentPCB, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE))
                    .addComponent(lblStudentPCM, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblStudentC, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblStudentM, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblStudentP, javax.swing.GroupLayout.DEFAULT_SIZE, 265, Short.MAX_VALUE)
                    .addComponent(lblStudentB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout comboLayout = new javax.swing.GroupLayout(combo);
        combo.setLayout(comboLayout);
        comboLayout.setHorizontalGroup(
            comboLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(comboLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jButton1)
                .addGap(30, 30, 30)
                .addGroup(comboLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblcombo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        comboLayout.setVerticalGroup(
            comboLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(comboLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(comboLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(comboLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(30, 30, 30)
                        .addComponent(lblcombo, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(290, 290, 290))
        );

        home.add(combo, "card3");

        product.setBackground(new java.awt.Color(255, 255, 255));
        product.setName("product"); // NOI18N

        jLabel11.setBackground(new java.awt.Color(204, 204, 255));
        jLabel11.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel11.setText("  MH-CET Preparation Software   ");
        jLabel11.setName("jLabel11"); // NOI18N
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel11MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel11MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel11MouseExited(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        jButton2.setBorder(null);
        jButton2.setBorderPainted(false);
        jButton2.setContentAreaFilled(false);
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton2.setName("jButton2"); // NOI18N
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton2MouseExited(evt);
            }
        });

        jLabel12.setBackground(new java.awt.Color(204, 204, 255));
        jLabel12.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel12.setText("  JEE/NEET Preparation Software   ");
        jLabel12.setName("jLabel12"); // NOI18N
        jLabel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel12MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel12MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel12MouseExited(evt);
            }
        });

        jLabel13.setBackground(new java.awt.Color(204, 204, 255));
        jLabel13.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel13.setText("  Client-Server Software");
        jLabel13.setName("jLabel13"); // NOI18N
        jLabel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel13MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel13MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel13MouseExited(evt);
            }
        });

        jLabel14.setBackground(new java.awt.Color(204, 204, 255));
        jLabel14.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel14.setText("  Time Table Management System  ");
        jLabel14.setName("jLabel14"); // NOI18N
        jLabel14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel14MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel14MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel14MouseExited(evt);
            }
        });

        jLabel15.setBackground(new java.awt.Color(204, 204, 255));
        jLabel15.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel15.setText("  Question Printing Software ");
        jLabel15.setName("jLabel15"); // NOI18N
        jLabel15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel15MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel15MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel15MouseExited(evt);
            }
        });

        jLabel16.setBackground(new java.awt.Color(204, 204, 255));
        jLabel16.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel16.setText("  Class Management System  ");
        jLabel16.setName("jLabel16"); // NOI18N
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel16MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel16MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel16MouseExited(evt);
            }
        });

        jLabel18.setBackground(new java.awt.Color(204, 204, 255));
        jLabel18.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel18.setText("  CruncherSoft ERP  ");
        jLabel18.setName("jLabel18"); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel18MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel18MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel18MouseExited(evt);
            }
        });

        jLabel19.setBackground(new java.awt.Color(204, 204, 255));
        jLabel19.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel19.setText("  Website Development  ");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel19MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel19MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel19MouseExited(evt);
            }
        });

        jLabel21.setBackground(new java.awt.Color(204, 204, 255));
        jLabel21.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel21.setText("  Online Preparation on www.cetonlinetest.com");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel21MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel21MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel21MouseExited(evt);
            }
        });

        javax.swing.GroupLayout productLayout = new javax.swing.GroupLayout(product);
        product.setLayout(productLayout);
        productLayout.setHorizontalGroup(
            productLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(productLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jButton2)
                .addGap(30, 30, 30)
                .addGroup(productLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19)
                    .addComponent(jLabel18)
                    .addComponent(jLabel11)
                    .addGroup(productLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.LEADING))
                    .addGroup(productLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING))
                    .addComponent(jLabel12)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 705, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(827, Short.MAX_VALUE))
        );
        productLayout.setVerticalGroup(
            productLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(productLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(productLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(productLayout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel12))
                    .addComponent(jButton2))
                .addGap(18, 18, 18)
                .addComponent(jLabel21)
                .addGap(18, 18, 18)
                .addComponent(jLabel15)
                .addGap(18, 18, 18)
                .addComponent(jLabel13)
                .addGap(16, 16, 16)
                .addComponent(jLabel16)
                .addGap(16, 16, 16)
                .addComponent(jLabel14)
                .addGap(18, 18, 18)
                .addComponent(jLabel18)
                .addGap(18, 18, 18)
                .addComponent(jLabel19)
                .addContainerGap())
        );

        home.add(product, "card4");

        jee.setBackground(new java.awt.Color(255, 255, 255));
        jee.setName("jee"); // NOI18N

        lbljeee.setBackground(new java.awt.Color(255, 255, 255));
        lbljeee.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lbljeee.setForeground(new java.awt.Color(255, 0, 0));
        lbljeee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/jee.png"))); // NOI18N
        lbljeee.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lbljeee.setName("lbljeee"); // NOI18N
        lbljeee.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lbljee.setBackground(new java.awt.Color(255, 255, 255));
        lbljee.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 18)); // NOI18N
        lbljee.setForeground(new java.awt.Color(239, 148, 51));
        lbljee.setName("lbljee"); // NOI18N

        jLabel17.setBackground(new java.awt.Color(204, 204, 255));
        jLabel17.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel17.setText("  JEE/NEET Preparation Software ");
        jLabel17.setName("jLabel17"); // NOI18N

        backjee.setBackground(new java.awt.Color(255, 255, 255));
        backjee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        backjee.setBorder(null);
        backjee.setBorderPainted(false);
        backjee.setContentAreaFilled(false);
        backjee.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        backjee.setName("backjee"); // NOI18N
        backjee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backjeeMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                backjeeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                backjeeMouseExited(evt);
            }
        });
        backjee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjeeActionPerformed(evt);
            }
        });

        lblneett.setBackground(new java.awt.Color(255, 255, 255));
        lblneett.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblneett.setForeground(new java.awt.Color(255, 0, 0));
        lblneett.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/net.png"))); // NOI18N
        lblneett.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblneett.setName("lblneett"); // NOI18N
        lblneett.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout jeeLayout = new javax.swing.GroupLayout(jee);
        jee.setLayout(jeeLayout);
        jeeLayout.setHorizontalGroup(
            jeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jeeLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(backjee)
                .addGap(30, 30, 30)
                .addGroup(jeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jeeLayout.createSequentialGroup()
                        .addComponent(lbljee, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49)
                        .addComponent(lbljeee)
                        .addGap(32, 32, 32)
                        .addComponent(lblneett))
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 574, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jeeLayout.setVerticalGroup(
            jeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jeeLayout.createSequentialGroup()
                .addGroup(jeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jeeLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblneett, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jeeLayout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(jeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(backjee)
                            .addGroup(jeeLayout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addGap(30, 30, 30)
                                .addComponent(lbljee, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jeeLayout.createSequentialGroup()
                                .addGap(118, 118, 118)
                                .addComponent(lbljeee, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(382, Short.MAX_VALUE))
        );

        home.add(jee, "card5");

        Clientserver.setBackground(new java.awt.Color(255, 255, 255));
        Clientserver.setName("Clientserver"); // NOI18N

        jLabel10.setBackground(new java.awt.Color(255, 255, 255));
        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 0, 0));
        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/CS.png"))); // NOI18N
        jLabel10.setText("Rs. 40,000/-");
        jLabel10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel10.setName("jLabel10"); // NOI18N
        jLabel10.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblcs.setBackground(new java.awt.Color(255, 255, 255));
        lblcs.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 18)); // NOI18N
        lblcs.setForeground(new java.awt.Color(21, 121, 65));
        lblcs.setName("lblcs"); // NOI18N

        jLabel20.setBackground(new java.awt.Color(204, 204, 255));
        jLabel20.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel20.setText("  Client-Server Software");
        jLabel20.setName("jLabel20"); // NOI18N

        jButton4.setBackground(new java.awt.Color(255, 255, 255));
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        jButton4.setBorder(null);
        jButton4.setBorderPainted(false);
        jButton4.setContentAreaFilled(false);
        jButton4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton4.setName("jButton4"); // NOI18N
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton4MouseExited(evt);
            }
        });
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ClientserverLayout = new javax.swing.GroupLayout(Clientserver);
        Clientserver.setLayout(ClientserverLayout);
        ClientserverLayout.setHorizontalGroup(
            ClientserverLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ClientserverLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jButton4)
                .addGap(30, 30, 30)
                .addGroup(ClientserverLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel20)
                    .addGroup(ClientserverLayout.createSequentialGroup()
                        .addComponent(lblcs, javax.swing.GroupLayout.PREFERRED_SIZE, 716, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(jLabel10)))
                .addGap(28, 28, 28))
        );
        ClientserverLayout.setVerticalGroup(
            ClientserverLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ClientserverLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(ClientserverLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ClientserverLayout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addGap(30, 30, 30)
                        .addComponent(lblcs, javax.swing.GroupLayout.PREFERRED_SIZE, 517, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(ClientserverLayout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton4))
                .addContainerGap(286, Short.MAX_VALUE))
        );

        home.add(Clientserver, "card6");

        printing.setBackground(new java.awt.Color(255, 255, 255));
        printing.setName("printing"); // NOI18N

        lblPRPCMB.setBackground(new java.awt.Color(255, 255, 255));
        lblPRPCMB.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        lblPRPCMB.setForeground(new java.awt.Color(255, 0, 0));
        lblPRPCMB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PRPCMB.png"))); // NOI18N
        lblPRPCMB.setText("Rs. 25,000/-");
        lblPRPCMB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPRPCMB.setName("lblPRPCMB"); // NOI18N
        lblPRPCMB.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblprint.setBackground(new java.awt.Color(255, 255, 255));
        lblprint.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 18)); // NOI18N
        lblprint.setForeground(new java.awt.Color(164, 53, 42));
        lblprint.setName("lblprint"); // NOI18N

        jLabel22.setBackground(new java.awt.Color(204, 204, 255));
        jLabel22.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel22.setText("  Question Printing Software   ");
        jLabel22.setName("jLabel22"); // NOI18N

        backpaper.setBackground(new java.awt.Color(255, 255, 255));
        backpaper.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        backpaper.setBorder(null);
        backpaper.setBorderPainted(false);
        backpaper.setContentAreaFilled(false);
        backpaper.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        backpaper.setName("backpaper"); // NOI18N
        backpaper.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backpaperMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                backpaperMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                backpaperMouseExited(evt);
            }
        });
        backpaper.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backpaperActionPerformed(evt);
            }
        });

        lblPRPCB.setBackground(new java.awt.Color(255, 255, 255));
        lblPRPCB.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        lblPRPCB.setForeground(new java.awt.Color(255, 0, 0));
        lblPRPCB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PRPCB.png"))); // NOI18N
        lblPRPCB.setText("Rs. 25,000/-");
        lblPRPCB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPRPCB.setName("lblPRPCB"); // NOI18N
        lblPRPCB.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblPRPCM.setBackground(new java.awt.Color(255, 255, 255));
        lblPRPCM.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        lblPRPCM.setForeground(new java.awt.Color(255, 0, 0));
        lblPRPCM.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PRPCM.png"))); // NOI18N
        lblPRPCM.setText("Rs. 25,000/-");
        lblPRPCM.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPRPCM.setName("lblPRPCM"); // NOI18N
        lblPRPCM.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblPRB.setBackground(new java.awt.Color(255, 255, 255));
        lblPRB.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        lblPRB.setForeground(new java.awt.Color(255, 0, 0));
        lblPRB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PRB.png"))); // NOI18N
        lblPRB.setText("Rs. 25,000/-");
        lblPRB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPRB.setName("lblPRB"); // NOI18N
        lblPRB.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblPRC.setBackground(new java.awt.Color(255, 255, 255));
        lblPRC.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        lblPRC.setForeground(new java.awt.Color(255, 0, 0));
        lblPRC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PRC.png"))); // NOI18N
        lblPRC.setText("Rs. 25,000/-");
        lblPRC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPRC.setName("lblPRC"); // NOI18N
        lblPRC.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblPRM.setBackground(new java.awt.Color(255, 255, 255));
        lblPRM.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        lblPRM.setForeground(new java.awt.Color(255, 0, 0));
        lblPRM.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PRM.png"))); // NOI18N
        lblPRM.setText("Rs. 25,000/-");
        lblPRM.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPRM.setName("lblPRM"); // NOI18N
        lblPRM.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblPRP.setBackground(new java.awt.Color(255, 255, 255));
        lblPRP.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        lblPRP.setForeground(new java.awt.Color(255, 0, 0));
        lblPRP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/PRB.png"))); // NOI18N
        lblPRP.setText("Rs. 25,000/-");
        lblPRP.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPRP.setName("lblPRP"); // NOI18N
        lblPRP.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout printingLayout = new javax.swing.GroupLayout(printing);
        printing.setLayout(printingLayout);
        printingLayout.setHorizontalGroup(
            printingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(printingLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(backpaper)
                .addGap(30, 30, 30)
                .addGroup(printingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblprint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel22))
                .addGap(27, 27, 27)
                .addGroup(printingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(printingLayout.createSequentialGroup()
                        .addComponent(lblPRP, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblPRC, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblPRM, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblPRB))
                    .addGroup(printingLayout.createSequentialGroup()
                        .addComponent(lblPRPCM)
                        .addGap(15, 15, 15)
                        .addComponent(lblPRPCMB)
                        .addGap(15, 15, 15)
                        .addComponent(lblPRPCB)
                        .addGap(77, 77, 77))))
        );
        printingLayout.setVerticalGroup(
            printingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(printingLayout.createSequentialGroup()
                .addGroup(printingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(printingLayout.createSequentialGroup()
                        .addGroup(printingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblPRPCM, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPRPCMB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPRPCB, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(printingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblPRM, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPRC, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPRP, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPRB, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(printingLayout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(printingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(printingLayout.createSequentialGroup()
                                .addComponent(jLabel22)
                                .addGap(30, 30, 30)
                                .addComponent(lblprint, javax.swing.GroupLayout.PREFERRED_SIZE, 438, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(backpaper))))
                .addGap(308, 308, 308))
        );

        home.add(printing, "card7");

        school.setBackground(new java.awt.Color(255, 255, 255));
        school.setName("school"); // NOI18N

        lblsc.setBackground(new java.awt.Color(255, 255, 255));
        lblsc.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 18)); // NOI18N
        lblsc.setForeground(new java.awt.Color(0, 143, 207));
        lblsc.setName("lblsc"); // NOI18N

        jLabel24.setBackground(new java.awt.Color(204, 204, 255));
        jLabel24.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel24.setText("Class Management System  ");
        jLabel24.setName("jLabel24"); // NOI18N

        jButton6.setBackground(new java.awt.Color(255, 255, 255));
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        jButton6.setBorder(null);
        jButton6.setBorderPainted(false);
        jButton6.setContentAreaFilled(false);
        jButton6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton6.setName("jButton6"); // NOI18N
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton6MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton6MouseExited(evt);
            }
        });
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jLabel31.setBackground(new java.awt.Color(255, 255, 255));
        jLabel31.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 0, 0));
        jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/CM.png"))); // NOI18N
        jLabel31.setText("Rs. 35,000/-");
        jLabel31.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel31.setName("jLabel31"); // NOI18N
        jLabel31.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout schoolLayout = new javax.swing.GroupLayout(school);
        school.setLayout(schoolLayout);
        schoolLayout.setHorizontalGroup(
            schoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(schoolLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jButton6)
                .addGap(30, 30, 30)
                .addGroup(schoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(schoolLayout.createSequentialGroup()
                        .addComponent(lblsc, javax.swing.GroupLayout.PREFERRED_SIZE, 606, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(86, 86, 86)
                        .addComponent(jLabel31))
                    .addComponent(jLabel24))
                .addContainerGap(628, Short.MAX_VALUE))
        );
        schoolLayout.setVerticalGroup(
            schoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(schoolLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(schoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(schoolLayout.createSequentialGroup()
                        .addComponent(jLabel24)
                        .addGroup(schoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(schoolLayout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(lblsc, javax.swing.GroupLayout.PREFERRED_SIZE, 629, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(schoolLayout.createSequentialGroup()
                                .addGap(118, 118, 118)
                                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jButton6))
                .addContainerGap(174, Short.MAX_VALUE))
        );

        home.add(school, "card8");

        timetable.setBackground(new java.awt.Color(255, 255, 255));
        timetable.setName("timetable"); // NOI18N

        lbltable.setBackground(new java.awt.Color(255, 255, 255));
        lbltable.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 20)); // NOI18N
        lbltable.setForeground(new java.awt.Color(51, 51, 51));
        lbltable.setName("lbltable"); // NOI18N

        jLabel26.setBackground(new java.awt.Color(204, 204, 255));
        jLabel26.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel26.setText("Time Table Management System  ");
        jLabel26.setName("jLabel26"); // NOI18N

        jButton7.setBackground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        jButton7.setBorder(null);
        jButton7.setBorderPainted(false);
        jButton7.setContentAreaFilled(false);
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton7.setName("jButton7"); // NOI18N
        jButton7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton7MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton7MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton7MouseExited(evt);
            }
        });
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jLabel7.setBackground(new java.awt.Color(255, 255, 255));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/timetable.png"))); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N

        javax.swing.GroupLayout timetableLayout = new javax.swing.GroupLayout(timetable);
        timetable.setLayout(timetableLayout);
        timetableLayout.setHorizontalGroup(
            timetableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(timetableLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jButton7)
                .addGap(30, 30, 30)
                .addGroup(timetableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel26)
                    .addGroup(timetableLayout.createSequentialGroup()
                        .addComponent(lbltable, javax.swing.GroupLayout.PREFERRED_SIZE, 544, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)))
                .addContainerGap(574, Short.MAX_VALUE))
        );
        timetableLayout.setVerticalGroup(
            timetableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(timetableLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(timetableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(timetableLayout.createSequentialGroup()
                        .addComponent(jLabel26)
                        .addGap(30, 30, 30)
                        .addGroup(timetableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(lbltable, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton7))
                .addContainerGap(419, Short.MAX_VALUE))
        );

        home.add(timetable, "card9");

        erp.setBackground(new java.awt.Color(255, 255, 255));
        erp.setName("erp"); // NOI18N

        jLabel27.setBackground(new java.awt.Color(255, 255, 255));
        jLabel27.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 0, 0));
        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/erp.png"))); // NOI18N
        jLabel27.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel27.setName("jLabel27"); // NOI18N
        jLabel27.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblerp.setBackground(new java.awt.Color(255, 255, 255));
        lblerp.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 20)); // NOI18N
        lblerp.setForeground(new java.awt.Color(209, 68, 137));
        lblerp.setName("lblerp"); // NOI18N

        jLabel28.setBackground(new java.awt.Color(204, 204, 255));
        jLabel28.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel28.setText("CruncherSoft ERP  ");
        jLabel28.setName("jLabel28"); // NOI18N

        jButton8.setBackground(new java.awt.Color(255, 255, 255));
        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        jButton8.setBorder(null);
        jButton8.setBorderPainted(false);
        jButton8.setContentAreaFilled(false);
        jButton8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton8.setName("jButton8"); // NOI18N
        jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton8MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton8MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton8MouseExited(evt);
            }
        });
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout erpLayout = new javax.swing.GroupLayout(erp);
        erp.setLayout(erpLayout);
        erpLayout.setHorizontalGroup(
            erpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(erpLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jButton8)
                .addGap(30, 30, 30)
                .addGroup(erpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(erpLayout.createSequentialGroup()
                        .addComponent(lblerp, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel27))
                    .addComponent(jLabel28))
                .addContainerGap(642, Short.MAX_VALUE))
        );
        erpLayout.setVerticalGroup(
            erpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(erpLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(erpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(erpLayout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addGap(30, 30, 30)
                        .addGroup(erpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel27)
                            .addComponent(lblerp, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton8))
                .addContainerGap(419, Short.MAX_VALUE))
        );

        home.add(erp, "card10");

        website.setBackground(new java.awt.Color(255, 255, 255));
        website.setName("website"); // NOI18N

        jLabel29.setBackground(new java.awt.Color(255, 255, 255));
        jLabel29.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 0, 0));
        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/website-icon.jpg"))); // NOI18N
        jLabel29.setToolTipText("");
        jLabel29.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel29.setName("jLabel29"); // NOI18N
        jLabel29.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        jLabel30.setBackground(new java.awt.Color(204, 204, 255));
        jLabel30.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel30.setText("Website Development  ");
        jLabel30.setName("jLabel30"); // NOI18N

        jButton9.setBackground(new java.awt.Color(255, 255, 255));
        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        jButton9.setBorder(null);
        jButton9.setBorderPainted(false);
        jButton9.setContentAreaFilled(false);
        jButton9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton9.setName("jButton9"); // NOI18N
        jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton9MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton9MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton9MouseExited(evt);
            }
        });
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        btnweb1.setBackground(new java.awt.Color(255, 170, 1));
        btnweb1.setFont(new java.awt.Font("Microsoft YaHei UI", 1, 20)); // NOI18N
        btnweb1.setForeground(new java.awt.Color(255, 170, 1));
        btnweb1.setBorderPainted(false);
        btnweb1.setContentAreaFilled(false);
        btnweb1.setName("btnweb1"); // NOI18N
        btnweb1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnweb1ActionPerformed(evt);
            }
        });

        btnweb0.setBackground(new java.awt.Color(255, 170, 1));
        btnweb0.setFont(new java.awt.Font("Microsoft YaHei UI", 1, 20)); // NOI18N
        btnweb0.setForeground(new java.awt.Color(255, 170, 1));
        btnweb0.setBorderPainted(false);
        btnweb0.setContentAreaFilled(false);
        btnweb0.setName("btnweb0"); // NOI18N
        btnweb0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnweb0ActionPerformed(evt);
            }
        });

        btnweb2.setBackground(new java.awt.Color(255, 170, 1));
        btnweb2.setFont(new java.awt.Font("Microsoft YaHei UI", 1, 20)); // NOI18N
        btnweb2.setForeground(new java.awt.Color(255, 170, 1));
        btnweb2.setBorderPainted(false);
        btnweb2.setContentAreaFilled(false);
        btnweb2.setName("btnweb2"); // NOI18N
        btnweb2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnweb2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout websiteLayout = new javax.swing.GroupLayout(website);
        website.setLayout(websiteLayout);
        websiteLayout.setHorizontalGroup(
            websiteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(websiteLayout.createSequentialGroup()
                .addGroup(websiteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(websiteLayout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jButton9)
                        .addGap(30, 30, 30)
                        .addComponent(jLabel30))
                    .addGroup(websiteLayout.createSequentialGroup()
                        .addGap(101, 101, 101)
                        .addGroup(websiteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnweb2, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnweb1, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnweb0, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(130, 130, 130)
                        .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(454, 454, 454))
        );
        websiteLayout.setVerticalGroup(
            websiteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(websiteLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(websiteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton9)
                    .addComponent(jLabel30))
                .addGap(163, 163, 163)
                .addGroup(websiteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(websiteLayout.createSequentialGroup()
                        .addComponent(btnweb0, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnweb1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnweb2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(301, Short.MAX_VALUE))
        );

        home.add(website, "card11");

        contactus.setBackground(new java.awt.Color(255, 255, 255));
        contactus.setName("contactus"); // NOI18N

        jLabel32.setBackground(new java.awt.Color(255, 255, 255));
        jLabel32.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(204, 0, 51));
        jLabel32.setText("Head Office");
        jLabel32.setName("jLabel32"); // NOI18N

        lbladdress.setBackground(new java.awt.Color(255, 255, 255));
        lbladdress.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        lbladdress.setForeground(new java.awt.Color(0, 143, 207));
        lbladdress.setName("lbladdress"); // NOI18N

        btnbackaddress.setBackground(new java.awt.Color(255, 255, 255));
        btnbackaddress.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        btnbackaddress.setBorder(null);
        btnbackaddress.setBorderPainted(false);
        btnbackaddress.setContentAreaFilled(false);
        btnbackaddress.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnbackaddress.setName("btnbackaddress"); // NOI18N
        btnbackaddress.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnbackaddressMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnbackaddressMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnbackaddressMouseExited(evt);
            }
        });
        btnbackaddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbackaddressActionPerformed(evt);
            }
        });

        btnwebsite.setBackground(new java.awt.Color(255, 170, 1));
        btnwebsite.setFont(new java.awt.Font("Microsoft YaHei UI", 1, 20)); // NOI18N
        btnwebsite.setForeground(new java.awt.Color(255, 170, 1));
        btnwebsite.setBorderPainted(false);
        btnwebsite.setContentAreaFilled(false);
        btnwebsite.setName("btnwebsite"); // NOI18N
        btnwebsite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnwebsiteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout contactusLayout = new javax.swing.GroupLayout(contactus);
        contactus.setLayout(contactusLayout);
        contactusLayout.setHorizontalGroup(
            contactusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contactusLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(btnbackaddress)
                .addGap(30, 30, 30)
                .addGroup(contactusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel32)
                    .addComponent(lbladdress, javax.swing.GroupLayout.PREFERRED_SIZE, 534, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnwebsite, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(998, 998, 998))
        );
        contactusLayout.setVerticalGroup(
            contactusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contactusLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(contactusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnbackaddress)
                    .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(76, 76, 76)
                .addComponent(lbladdress, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnwebsite, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        home.add(contactus, "card12");

        aboutus.setBackground(new java.awt.Color(255, 255, 255));
        aboutus.setName("aboutus"); // NOI18N

        lblaboutus.setBackground(new java.awt.Color(255, 255, 255));
        lblaboutus.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 20)); // NOI18N
        lblaboutus.setForeground(new java.awt.Color(0, 143, 207));
        lblaboutus.setName("lblaboutus"); // NOI18N

        btnbackaboutus.setBackground(new java.awt.Color(255, 255, 255));
        btnbackaboutus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        btnbackaboutus.setBorder(null);
        btnbackaboutus.setBorderPainted(false);
        btnbackaboutus.setContentAreaFilled(false);
        btnbackaboutus.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnbackaboutus.setName("btnbackaboutus"); // NOI18N
        btnbackaboutus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnbackaboutusMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnbackaboutusMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnbackaboutusMouseExited(evt);
            }
        });
        btnbackaboutus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbackaboutusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout aboutusLayout = new javax.swing.GroupLayout(aboutus);
        aboutus.setLayout(aboutusLayout);
        aboutusLayout.setHorizontalGroup(
            aboutusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(aboutusLayout.createSequentialGroup()
                .addGroup(aboutusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(aboutusLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(btnbackaboutus))
                    .addGroup(aboutusLayout.createSequentialGroup()
                        .addGap(138, 138, 138)
                        .addComponent(lblaboutus, javax.swing.GroupLayout.PREFERRED_SIZE, 894, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(601, Short.MAX_VALUE))
        );
        aboutusLayout.setVerticalGroup(
            aboutusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(aboutusLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(btnbackaboutus)
                .addGap(18, 18, 18)
                .addComponent(lblaboutus, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(348, Short.MAX_VALUE))
        );

        home.add(aboutus, "card13");

        cetonline.setBackground(new java.awt.Color(255, 255, 255));
        cetonline.setName("cetonline"); // NOI18N

        lblcombo1.setBackground(new java.awt.Color(255, 255, 255));
        lblcombo1.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 18)); // NOI18N
        lblcombo1.setForeground(new java.awt.Color(0, 143, 207));
        lblcombo1.setName("lblcombo1"); // NOI18N

        jLabel9.setBackground(new java.awt.Color(204, 204, 255));
        jLabel9.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/cats-crop.jpg"))); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N

        btnbackonline.setBackground(new java.awt.Color(255, 255, 255));
        btnbackonline.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N
        btnbackonline.setBorder(null);
        btnbackonline.setBorderPainted(false);
        btnbackonline.setContentAreaFilled(false);
        btnbackonline.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnbackonline.setName("btnbackonline"); // NOI18N
        btnbackonline.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnbackonlineMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnbackonlineMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnbackonlineMouseExited(evt);
            }
        });
        btnbackonline.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbackonlineActionPerformed(evt);
            }
        });

        btnweb3.setBackground(new java.awt.Color(255, 170, 1));
        btnweb3.setFont(new java.awt.Font("Microsoft YaHei UI", 1, 20)); // NOI18N
        btnweb3.setForeground(new java.awt.Color(255, 170, 1));
        btnweb3.setBorderPainted(false);
        btnweb3.setContentAreaFilled(false);
        btnweb3.setName("btnweb3"); // NOI18N
        btnweb3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnweb3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout cetonlineLayout = new javax.swing.GroupLayout(cetonline);
        cetonline.setLayout(cetonlineLayout);
        cetonlineLayout.setHorizontalGroup(
            cetonlineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cetonlineLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(btnbackonline)
                .addGap(30, 30, 30)
                .addGroup(cetonlineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblcombo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(btnweb3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        cetonlineLayout.setVerticalGroup(
            cetonlineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cetonlineLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(cetonlineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnbackonline)
                    .addGroup(cetonlineLayout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(30, 30, 30)
                        .addComponent(lblcombo1, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(33, 33, 33)
                .addComponent(btnweb3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(80, 80, 80))
        );

        home.add(cetonline, "card15");

        jScrollPane1.setViewportView(home);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void LblBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackMouseClicked
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card4");
            LblMainBack.setVisible(false);
            
            break;
        }
    }

}//GEN-LAST:event_LblBackMouseClicked

private void jButton1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseEntered
// TODO add your handling code here:
    jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png"))); // NOI18N
}//GEN-LAST:event_jButton1MouseEntered

private void jButton1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseExited
    jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png"))); // NOI18N// TODO add your handling code here:
}//GEN-LAST:event_jButton1MouseExited

private void LblBackMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackMouseEntered
// TODO add your handling code here:
    LblBack.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_LblBackMouseEntered

private void LblBackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackMouseExited
// TODO add your handling code here:

    LblBack.setBorder(null);
}//GEN-LAST:event_LblBackMouseExited

private void jLabel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseEntered
// TODO add your handling code here:
    jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel5MouseEntered

private void jLabel5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseExited
// TODO add your handling code here:
    jLabel5.setBorder(null);
}//GEN-LAST:event_jLabel5MouseExited

private void jLabel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseEntered
// TODO add your handling code here:
    jLabel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel2MouseEntered

private void jLabel2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseExited
// TODO add your handling code here:
    jLabel2.setBorder(null);
}//GEN-LAST:event_jLabel2MouseExited

private void jButton2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseEntered
// TODO add your handling code here:
    jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
}//GEN-LAST:event_jButton2MouseEntered

private void jButton2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseExited
// TODO add your handling code here:
    jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
}//GEN-LAST:event_jButton2MouseExited

private void jLabel11MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseEntered
// TODO add your handling code here:
    jLabel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel11MouseEntered

private void jLabel13MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MouseEntered
// TODO add your handling code here:
    jLabel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel13MouseEntered

private void jLabel11MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseExited
// TODO add your handling code here:
    jLabel11.setBorder(null);
}//GEN-LAST:event_jLabel11MouseExited

private void jLabel13MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MouseExited
// TODO add your handling code here:
    jLabel13.setBorder(null);
}//GEN-LAST:event_jLabel13MouseExited

private void jLabel12MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseEntered
// TODO add your handling code here:
    jLabel12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel12MouseEntered

private void jLabel12MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseExited
// TODO add your handling code here:
    jLabel12.setBorder(null);
}//GEN-LAST:event_jLabel12MouseExited

private void jLabel15MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel15MouseEntered
// TODO add your handling code here:
    jLabel15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel15MouseEntered

private void jLabel15MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel15MouseExited
// TODO add your handling code here:
    jLabel15.setBorder(null);
}//GEN-LAST:event_jLabel15MouseExited

private void jLabel16MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseEntered
// TODO add your handling code here:
    jLabel16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel16MouseEntered

private void jLabel16MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseExited
// TODO add your handling code here:
    jLabel16.setBorder(null);
}//GEN-LAST:event_jLabel16MouseExited

private void jLabel14MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel14MouseEntered
// TODO add your handling code here:
    jLabel14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel14MouseEntered

private void jLabel14MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel14MouseExited
// TODO add your handling code here:
    jLabel14.setBorder(null);
}//GEN-LAST:event_jLabel14MouseExited

private void jLabel18MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseEntered
// TODO add your handling code here:
    jLabel18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel18MouseEntered

private void jLabel18MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseExited
// TODO add your handling code here:
    jLabel18.setBorder(null);
}//GEN-LAST:event_jLabel18MouseExited

private void jLabel19MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseEntered
// TODO add your handling code here:
    jLabel19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
}//GEN-LAST:event_jLabel19MouseEntered

private void jLabel19MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseExited
// TODO add your handling code here:
    jLabel19.setBorder(null);
}//GEN-LAST:event_jLabel19MouseExited

private void jLabel11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseClicked
    
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card3");
            break;
        }
    }// TODO add your handling code here:
}//GEN-LAST:event_jLabel11MouseClicked

private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card2");
            LblMainBack.setVisible(true);
            break;
        }
    }// TODO add your handling code here:
}//GEN-LAST:event_jButton2MouseClicked

private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
// TODO add your handling code here:
}//GEN-LAST:event_jButton1MouseClicked

private void backjeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backjeeMouseClicked
// TODO add your handling code here:
}//GEN-LAST:event_backjeeMouseClicked

private void backjeeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backjeeMouseEntered
// TODO add your handling code here:
    backjee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
}//GEN-LAST:event_backjeeMouseEntered

private void backjeeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backjeeMouseExited
// TODO add your handling code here:
    backjee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
}//GEN-LAST:event_backjeeMouseExited

private void jLabel12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseClicked
    // TODO add your handling code here:

    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card5");
            break;
        }
    }

}//GEN-LAST:event_jLabel12MouseClicked

private void backjeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjeeActionPerformed
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card4");
            break;
        }
    }
}//GEN-LAST:event_backjeeActionPerformed

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
// TODO add your handling code here:
 
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card4");
            break;
        }
    }
}//GEN-LAST:event_jButton1ActionPerformed

private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseClicked
// TODO add your handling code here:
}//GEN-LAST:event_jButton4MouseClicked

private void jButton4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseEntered
// TODO add your handling code here:
    jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
}//GEN-LAST:event_jButton4MouseEntered

private void jButton4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseExited
// TODO add your handling code here:
    jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
}//GEN-LAST:event_jButton4MouseExited

private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card4");
            break;
        }
    }
}//GEN-LAST:event_jButton4ActionPerformed

private void jButton6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseClicked
// TODO add your handling code here:
}//GEN-LAST:event_jButton6MouseClicked

private void jButton6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseEntered
// TODO add your handling code here:
    jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
}//GEN-LAST:event_jButton6MouseEntered

private void jButton6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseExited
// TODO add your handling code here:
    jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
}//GEN-LAST:event_jButton6MouseExited

private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card4");
            break;
        }
    }
}//GEN-LAST:event_jButton6ActionPerformed

private void jButton7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton7MouseClicked
// TODO add your handling code here:
}//GEN-LAST:event_jButton7MouseClicked

private void jButton7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton7MouseEntered
// TODO add your handling code here:
    jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
}//GEN-LAST:event_jButton7MouseEntered

private void jButton7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton7MouseExited
// TODO add your handling code here:
    jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
}//GEN-LAST:event_jButton7MouseExited

private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card4");
            break;
        }
    }
}//GEN-LAST:event_jButton7ActionPerformed

private void jButton8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton8MouseClicked
// TODO add your handling code here:
}//GEN-LAST:event_jButton8MouseClicked

private void jButton8MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton8MouseEntered
// TODO add your handling code here:
    jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
}//GEN-LAST:event_jButton8MouseEntered

private void jButton8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton8MouseExited
// TODO add your handling code here:
    jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
}//GEN-LAST:event_jButton8MouseExited

private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card4");
            break;
        }
    }
}//GEN-LAST:event_jButton8ActionPerformed

private void jButton9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MouseClicked
// TODO add your handling code here:
}//GEN-LAST:event_jButton9MouseClicked

private void jButton9MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MouseEntered
// TODO add your handling code here:
    jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
}//GEN-LAST:event_jButton9MouseEntered

private void jButton9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MouseExited
// TODO add your handling code here:
    jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
}//GEN-LAST:event_jButton9MouseExited

private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card4");
            break;
        }
    }
}//GEN-LAST:event_jButton9ActionPerformed

private void jLabel13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MouseClicked
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card6");
            break;
        }
    }
}//GEN-LAST:event_jLabel13MouseClicked

private void jLabel18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseClicked
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card10");
            break;
        }
    }
}//GEN-LAST:event_jLabel18MouseClicked

private void jLabel19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseClicked
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card11");
            break;
        }
    }
}//GEN-LAST:event_jLabel19MouseClicked

private void jLabel15MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel15MouseClicked
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card7");
            break;
        }
    }
}//GEN-LAST:event_jLabel15MouseClicked

private void jLabel16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseClicked
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card8");
            break;
        }
    }
}//GEN-LAST:event_jLabel16MouseClicked

private void jLabel14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel14MouseClicked
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card9");
            break;
        }
    }
}//GEN-LAST:event_jLabel14MouseClicked

private void btnweb0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnweb0ActionPerformed
// TODO add your handling code here:
    open(uri);
}//GEN-LAST:event_btnweb0ActionPerformed

private void btnweb1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnweb1ActionPerformed
// TODO add your handling code here:
    open(uri1);
}//GEN-LAST:event_btnweb1ActionPerformed

private void btnweb2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnweb2ActionPerformed
// TODO add your handling code here:
    open(uri2);
}//GEN-LAST:event_btnweb2ActionPerformed

private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card12");
            LblMainBack.setVisible(false);
            break;
        }
    }

}//GEN-LAST:event_jLabel2MouseClicked

private void btnbackaddressMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackaddressMouseClicked
// 
}//GEN-LAST:event_btnbackaddressMouseClicked

private void btnbackaddressMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackaddressMouseEntered
// TODO add your handling code here:
    btnbackaddress.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));

}//GEN-LAST:event_btnbackaddressMouseEntered

private void btnbackaddressMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackaddressMouseExited
// TODO add your handling code here:
    btnbackaddress.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
}//GEN-LAST:event_btnbackaddressMouseExited

private void btnbackaddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbackaddressActionPerformed
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card2");
            LblMainBack.setVisible(true);
            break;
        }
    }
}//GEN-LAST:event_btnbackaddressActionPerformed

private void btnwebsiteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnwebsiteActionPerformed
// TODO add your handling code here:
    open(uri3);
}//GEN-LAST:event_btnwebsiteActionPerformed

private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
// TODO add your handling code here:
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card13");
            LblMainBack.setVisible(false);
            break;
        }
    }
}//GEN-LAST:event_jLabel5MouseClicked

private void btnbackaboutusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackaboutusMouseClicked
// TODO add your handling code here:
}//GEN-LAST:event_btnbackaboutusMouseClicked

private void btnbackaboutusMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackaboutusMouseEntered
// TODO add your handling code here:
    btnbackaboutus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
}//GEN-LAST:event_btnbackaboutusMouseEntered

private void btnbackaboutusMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackaboutusMouseExited
// TODO add your handling code here:
    btnbackaboutus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
}//GEN-LAST:event_btnbackaboutusMouseExited

private void btnbackaboutusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbackaboutusActionPerformed
// TODO add your handling code here
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card2");
            LblMainBack.setVisible(true);
            break;
        }
    }
}//GEN-LAST:event_btnbackaboutusActionPerformed

private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
    Object[] options = {"YES", "CANCEL"};
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Exit Application?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    if (i == 0) {
//        new HomePage().setVisible(true);
        this.dispose();
    }
}//GEN-LAST:event_formWindowClosing

    private void backpaperActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backpaperActionPerformed
        // TODO add your handling code here:
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                cl.show(home, "card4");
                break;
            }
        }
    }//GEN-LAST:event_backpaperActionPerformed

    private void backpaperMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backpaperMouseExited
        // TODO add your handling code here:
        backpaper.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
    }//GEN-LAST:event_backpaperMouseExited

    private void backpaperMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backpaperMouseEntered
        // TODO add your handling code here:
        backpaper.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
    }//GEN-LAST:event_backpaperMouseEntered

    private void backpaperMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backpaperMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_backpaperMouseClicked

    private void jLabel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseClicked
        // TODO add your handling code here:
        
    switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card15");
            break;
        }
    }
    }//GEN-LAST:event_jLabel21MouseClicked

    private void jLabel21MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseEntered
        // TODO add your handling code here:
         jLabel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    }//GEN-LAST:event_jLabel21MouseEntered

    private void jLabel21MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseExited
        // TODO add your handling code here:
        jLabel21.setBorder(null);
    }//GEN-LAST:event_jLabel21MouseExited

    private void btnbackonlineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbackonlineActionPerformed
        // TODO add your handling code here:
        switch (evt.getModifiers()) {
        case InputEvent.BUTTON1_MASK: {
            cl.show(home, "card4");
            break;
        }
    }
    }//GEN-LAST:event_btnbackonlineActionPerformed

    private void btnbackonlineMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackonlineMouseExited
        // TODO add your handling code here:
         btnbackonline.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-2.png")));
    }//GEN-LAST:event_btnbackonlineMouseExited

    private void btnbackonlineMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackonlineMouseEntered
        // TODO add your handling code here:
         btnbackonline.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/aboutUs/BackSmall-1.png")));
    }//GEN-LAST:event_btnbackonlineMouseEntered

    private void btnbackonlineMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackonlineMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnbackonlineMouseClicked

    private void btnweb3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnweb3ActionPerformed
        // TODO add your handling code here:
        open(uri4);
    }//GEN-LAST:event_btnweb3ActionPerformed

    private void LblMainBackMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblMainBackMouseEntered
        // TODO add your handling code here:
        LblMainBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Back-2.png"))); // NOI18N
    }//GEN-LAST:event_LblMainBackMouseEntered

    private void LblMainBackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblMainBackMouseExited
        // TODO add your handling code here:
        LblMainBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Back-1.png"))); // NOI18N
    }//GEN-LAST:event_LblMainBackMouseExited

    private void LblMainBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblMainBackMouseClicked
        // TODO add your handling code here:
        newTestform.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_LblMainBackMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AboutUs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AboutUs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AboutUs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AboutUs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }//</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                try {
                    new AboutUs(1).setVisible(true);
                } catch (URISyntaxException ex) {
                    Logger.getLogger(AboutUs.class.getName()).log(Level.SEVERE, null, ex);
                }
                timer.start();
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BackBtnPanel;
    private javax.swing.JPanel Clientserver;
    private javax.swing.JLabel LblBack;
    private javax.swing.JLabel LblMainBack;
    private javax.swing.JPanel aboutus;
    private javax.swing.JButton backjee;
    private javax.swing.JButton backpaper;
    private javax.swing.JButton btnbackaboutus;
    private javax.swing.JButton btnbackaddress;
    private javax.swing.JButton btnbackonline;
    private javax.swing.JButton btnweb0;
    private javax.swing.JButton btnweb1;
    private javax.swing.JButton btnweb2;
    private javax.swing.JButton btnweb3;
    private javax.swing.JButton btnwebsite;
    private javax.swing.JPanel cetonline;
    private javax.swing.JPanel combo;
    private javax.swing.JPanel contactus;
    private javax.swing.JPanel erp;
    private javax.swing.JPanel home;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel jee;
    private javax.swing.JLabel lblPRB;
    private javax.swing.JLabel lblPRC;
    private javax.swing.JLabel lblPRM;
    private javax.swing.JLabel lblPRP;
    private javax.swing.JLabel lblPRPCB;
    private javax.swing.JLabel lblPRPCM;
    private javax.swing.JLabel lblPRPCMB;
    private javax.swing.JLabel lblStudentB;
    private javax.swing.JLabel lblStudentC;
    private javax.swing.JLabel lblStudentM;
    private javax.swing.JLabel lblStudentP;
    private javax.swing.JLabel lblStudentPCB;
    private javax.swing.JLabel lblStudentPCM;
    private javax.swing.JLabel lblStudentPCMB;
    private javax.swing.JLabel lblaboutus;
    private javax.swing.JLabel lbladdress;
    private javax.swing.JLabel lblcombo;
    private javax.swing.JLabel lblcombo1;
    private javax.swing.JLabel lblcs;
    private javax.swing.JLabel lblerp;
    private javax.swing.JLabel lbljee;
    private javax.swing.JLabel lbljeee;
    private javax.swing.JLabel lblneett;
    private javax.swing.JLabel lblprint;
    private javax.swing.JLabel lblsc;
    private javax.swing.JLabel lbltable;
    private javax.swing.JPanel printing;
    private javax.swing.JPanel product;
    private javax.swing.JPanel school;
    private javax.swing.JPanel timetable;
    private javax.swing.JPanel website;
    // End of variables declaration//GEN-END:variables
}
